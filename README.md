# Capon Springs
## v1.5

This is the WordPress theme for the [Capon Springs](http://www.caponsprings.net/)

## Base Framework

This theme is based off of the Genesis Framework by StudioPress. This is a complete rewrite of the original Capon theme that was developed by 7D Interactive.

## Changelog

### 1.5

* Bump up version number to avoid confusion with old theme
* Add class to allow items to be shown on mobile but hidden on wide displays

### 1.0.7

* Squash layout bugs in IE7 and 8

### 1.0.6

* Squash layout bugs in Safari

### 1.0.4

* Minor tweaks per design feedback

### 1.0.2

* Minor CSS and layout tweaks

### 1.0.1

* Complete rewrite to base the theme off of Genesis & make it easier to manage

## Contributors

* Designer: [White Spider Design](http://whitespiderdesign.com)
* Developer: [Ten-321 Enterprises](http://ten-321.com/)