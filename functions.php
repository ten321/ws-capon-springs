<?php
/**
 * The main function definitions for the Capon Springs 2016 theme
 * @version 1.5
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You do not have permission to access this file directly.' );
}

if ( ! class_exists( 'Capon_2016' ) ) {
	class Capon_2016 {
		/**
		 * Holds the version number for use with various assets
		 *
		 * @since  1.0.1
		 * @access public
		 * @var    string
		 */
		public $version = '1.5.3.2';
		
		/**
		 * Holds the class instance.
		 *
		 * @since   1.0.1
		 * @access	private
		 * @var		Capon_2016
		 */
		private static $instance;
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   1.0.1
		 * @return	Capon_2016
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className = __CLASS__;
				self::$instance = new $className;
			}
			return self::$instance;
		}
		
		/**
		 * Create the object and set up the appropriate actions
		 */
		private function __construct() {
			//* Child theme (do not remove)
			define( 'CHILD_THEME_NAME', 'Capon 2016' );
			define( 'CHILD_THEME_URL', 'https://www.caponsprings.net/' );
			define( 'CHILD_THEME_VERSION', $this->version );
			
			add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts_and_styles' ) );
			add_action( 'template_redirect', array( $this, 'template_redirect' ) );
			
			add_action( 'after_setup_theme', array( $this, 'adjust_genesis' ) );
			add_action( 'genesis_setup', array( $this, 'register_sidebars' ) );
			
			add_image_size( 'header-feature', 940, 285, true );
		}
		
		/**
		 * Adjust anything within Genesis that needs to be modified
		 */
		function adjust_genesis() {
			do_action( 'capon_pre_adjust_genesis' );
			
			remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
			add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );
			add_theme_support( 'genesis-responsive-viewport' );
			add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );
			add_theme_support( 'genesis-structural-wraps', array(
				'header',
				'nav',
				'subnav',
				'site-inner',
				'footer-widgets',
				'footer'
			) );
			add_theme_support( 'custom-background' );
			add_theme_support( 'genesis-menus' , array( 'primary' => __( 'Primary Navigation Menu', 'capon-2016' ) ) );
			remove_action( 'genesis_after_header', 'genesis_do_nav' );
			remove_action( 'genesis_after_header', 'genesis_do_subnav' );
			add_action( 'genesis_header', 'genesis_do_nav', 9 );
			
			unregister_sidebar( 'sidebar' );
			unregister_sidebar( 'sidebar-alt' );
			unregister_sidebar( 'header-right' );
			
			remove_all_actions( 'genesis_sidebar' );
			remove_all_actions( 'genesis_sidebar_alt' );
			add_action( 'genesis_sidebar', array( $this, 'do_sidebar' ) );
			
			remove_action( 'genesis_header', 'genesis_do_header' );
			add_action( 'genesis_header', array( $this, 'do_header' ) );
			remove_action( 'genesis_footer', 'genesis_do_footer' );
			add_action( 'genesis_footer', array( $this, 'do_footer' ) );
			
			do_action( 'capon_post_adjust_genesis' );
			
			foreach ( array( 'content-sidebar', 'content-sidebar-sidebar', 'sidebar-sidebar-content', 'sidebar-content-sidebar' ) as $l ) {
				genesis_unregister_layout( $l );
			}
			
			add_action( 'genesis_sidebar', array( $this, 'remove_jetpack_sharing' ), 1 );
		}
		
		function remove_jetpack_sharing() {
			if ( has_filter( 'the_content', 'sharing_display' ) ) {
				remove_filter( 'the_content', 'sharing_display', 19 );
				remove_filter( 'the_excerpt', 'sharing_display', 19 );
				add_action( 'genesis_sidebar', array( $this, 'add_jetpack_sharing' ), 99 );
			}
		}
		
		function add_jetpack_sharing() {
			add_filter( 'the_content', 'sharing_display', 19 );
			add_filter( 'the_excerpt', 'sharing_display', 19 );
		}
		
		/**
		 * Perform any changes that need to happen after we've identified the queried object
		 */
		function template_redirect() {
			if ( is_front_page() ) {
				remove_all_actions( 'genesis_loop' );
				add_action( 'genesis_loop', array( $this, 'do_front_page_widgets' ) );
			}
		}
		
		/**
		 * Register/enqueue any necessary style sheets/script files
		 */
		function add_scripts_and_styles() {
			wp_enqueue_script( 'capon-2016', get_stylesheet_directory_uri() . '/scripts/capon.js', array( 'jquery' ), $this->version, true );
			wp_register_style( 'genesis', get_stylesheet_uri(), array(), $this->version, 'all' );
			wp_enqueue_style( 'capon-2016', get_stylesheet_directory_uri() . '/capon.css', array( 'genesis' ), $this->version, 'all' );
		}
		
		/**
		 * Register any necessary widgetized areas/sidebars
		 */
		function register_sidebars() {
			$sidebars = apply_filters( 'capon_registered_sidebars', array(
				array(
					'id'          => 'sidebar-1', 
					'name'        => __( 'Primary Sidebar' ), 
					'description' => __( 'Appears anywhere the primary sidebar is set to appear' ), 
				), 
				array( 
					'id'          => 'sidebar-2', 
					'name'        => __( 'Blog Sidebar' ), 
					'description' => __( 'Appears directly above the primary sidebar on any blog archives or posts' ), 
				), 
				array(
					'id'          => 'header-main', 
					'name'        => __( 'Main Header Area' ), 
					'description' => __( 'Outputs the main content of the header area' ), 
				), 
				array(
					'id'          => 'footer-left', 
					'name'        => __( 'Primary Footer Area' ), 
					'description' => __( 'Appears on the left side of the footer; one-fourth the width of the screen' ), 
				), 
				array(
					'id'          => 'footer-right', 
					'name'        => __( 'Secondary Footer Area' ), 
					'description' => __( 'Appears on the right side of the footer; three-fourths the width of the screen' ), 
				), 
				array(
					'id'          => 'home-top', 
					'name'        => __( '[Front Page] Top' ), 
					'description' => __( 'Appears at the very top of the front page, full-width' ), 
				), 
				array( 
					'id'          => 'home-features', 
					'name'        => __( '[Front Page] Featured Items' ), 
					'description' => __( 'Appears on the front page below the content area; each widget is one-third width on desktop' ),  
				), 
				array(
					'id'          => 'home-above-footer', 
					'name'        => __( '[Front Page] Above Footer' ), 
					'description' => __( 'Appears at the very bottom of the front page; full-width' ), 
				), 
			) );
			
			foreach ( $sidebars as $s ) {
				genesis_register_sidebar( $s );
			}
		}
		
		/**
		 * Output the primary sidebar
		 */
		function do_sidebar() {
			$is_blog_page = false;
			if ( ( is_archive() && ! is_post_type_archive() ) || is_singular( 'post' ) )
				$is_blog_page = true;
			
			if ( $is_blog_page && is_active_sidebar( 'sidebar-2' ) ) {
				dynamic_sidebar( 'sidebar-2' );
			} else if ( is_singular( 'page' ) ) {
				$this->do_related_pages();
			}
			
			if ( is_active_sidebar( 'sidebar-1' ) ) {
				dynamic_sidebar( 'sidebar-1' );
			}
		}
		
		/**
		 * Output the related pages widget
		 */
		function do_related_pages() {
			if ( ! is_page() ) {
				return;
			}
			
			$page = get_queried_object();
			$args = array(
				'sort_column' => 'menu_order', 
				'title_li'    => '', 
				'echo'        => 0
			);
			if ( property_exists( $page, 'post_parent' ) && ! empty( $page->post_parent ) ) {
				$args['child_of'] = $page->post_parent;
			} else {
				$args['child_of'] = $page->ID;
			}
			
			$children = wp_list_pages( $args );
			if ( empty( $children ) || is_wp_error( $children ) )
				return;
			
			printf( '
		<div class="related sidebar panel">
			<h4 class="widgettitle widget-title">Related Pages</h5>
			<ul>
				%s
			</ul>
		</div>', $children );
		}
		
		/**
		 * Output the front page widget areas
		 */
		function do_front_page_widgets() {
			if ( is_active_sidebar( 'home-top' ) ) {
				echo '<section class="home-top">';
				dynamic_sidebar( 'home-top' );
				echo '</section>';
			}
			if ( is_active_sidebar( 'home-features' ) ) {
				echo '<section class="home-features">';
				dynamic_sidebar( 'home-features' );
				echo '</section>';
			}
			if ( is_active_sidebar( 'home-above-footer' ) ) {
				echo '<footer class="home-above-footer">';
				dynamic_sidebar( 'home-above-footer' );
				echo '</footer>';
			}
		}
		
		/**
		 * Output the custom Capon Springs header
		 */
		function do_header() {
			echo '<section class="header-main">';
			if ( is_front_page() ) {
				dynamic_sidebar( 'header-main' );
			/*} else if ( is_singular() && has_post_thumbnail() ) {
				printf( '<figure class="header-feature">%s</figure>', get_the_post_thumbnail( get_the_ID(), 'header-feature' ) );*/
			} else {
				dynamic_sidebar( 'header-main' );
			}
			echo '</section>';
		}
		
		/**
		 * Output the custom Capon Springs footer
		 */
		function do_footer() {
			echo '<div class="clearfix"><div class="one-fourth first footer-area-1">';
			dynamic_sidebar( 'footer-left' );
			echo '</div><div class="three-fourths footer-area-2">';
			dynamic_sidebar( 'footer-right' );
			$this->white_spider_notice();
			echo '</div></div>';
		}
		
		/**
		 * Output the White Spider notice
		 */
		function white_spider_notice() {
			printf( '<div id="white-spider-copyright-widget-1" class="widget"><div class="widget-wrap"><div class="textwidget">%s</div></div></div>', sprintf( __( 'Site by <a href="%s" rel="nofollow noindex">%s</a>' ), 'http://whitespiderinc.com/', 'White Spider' ) );
		}
	}
}

global $capon_2016;
$capon_2016 = Capon_2016::instance();